import { useState, useEffect } from "react";

import { IChartListItem } from "../../types/types";

import styles from "./List.module.css";

interface ListProps {
  dataFromApi: IChartListItem[];
  deleteFromList: (obj: IChartListItem) => void;
  loadMoreData: () => void;
}

export default function List({
  dataFromApi,
  deleteFromList,
  loadMoreData,
}: ListProps) {
  const [listElements, setListElements] = useState<JSX.Element[]>([]);

  let listDataFromApi;
  useEffect(() => {
    if (dataFromApi) {
      listDataFromApi = dataFromApi.map((el) => {
        return (
          <li key={el.id}>
            <div>
              <p className={styles.list_headline}>{el.value}</p>
              <p>{el.date}</p>
            </div>
            <button
              onClick={() => {
                deleteFromList(el);
              }}
            >
              <img src="./img/trash-icon.svg" alt="Delete button"></img>
            </button>
          </li>
        );
      });
      setListElements(listDataFromApi);
    }
  }, [dataFromApi]);

  return (
    <div>
      <div className={styles.list_header}>
        <div className={styles.left_side}>
          <p>Value</p>
        </div>
        <div className={styles.right_side}>
          <p>Date and time</p>
        </div>
      </div>
      <ul className={styles.main_list}>{listElements}</ul>

      <div className={styles.load_more_btn_wrapper}>
        <button onClick={loadMoreData}>Load next page</button>
      </div>
    </div>
  );
}
