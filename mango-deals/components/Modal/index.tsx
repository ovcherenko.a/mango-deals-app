import React, { SetStateAction, useState } from "react";

import styles from "./Modal.module.css";

export interface ModalProps {
  isModalOpen: boolean;
  addToList (formDate: string, formValue: string): void;
  modalHandler: () => void;
  successModalState: boolean;
  setSuccessModalState: (type: boolean) => void;
}

export default function Modal({
  isModalOpen,
  addToList,
  modalHandler,
  successModalState,
  setSuccessModalState,
}: ModalProps) {
  const [formDate, setFormDate] = useState(""); 
  const [formValue, setFormValue] = useState("");

  function handleDateChange(e: React.ChangeEvent<HTMLInputElement>) {
    setFormDate(e.target.value);
  }

  function handleValueChange(e: React.ChangeEvent<HTMLInputElement>) {
    setFormValue(e.target.value);
  }

  function formHandler(e: { preventDefault: () => void }) {
    e.preventDefault();
    addToList(formDate, formValue);
    setTimeout(() => {
      modalHandler();
      setSuccessModalState(false);
      setFormDate("");
      setFormValue("");
    }, 1500);
  }

  return (
    <div
      className={`${styles.modal} ${
        isModalOpen ? styles.modalActive : styles.modalDisable
      }`}
    >
      <div className={`${successModalState ? styles.hide : ""}`}>
        <div className={styles.headlineWrapper}>
          <p>Make a New Deal</p>
          <img
            onClick={modalHandler}
            src="./img/closeModal.svg"
            alt="Close modal"
          />
        </div>
        <form onSubmit={formHandler} className={styles.form}>
          <div className={styles.formWrapper}>
            <div>
              <label htmlFor="date">Current Date</label>
              <input
                id="date"
                type="date"
                value={formDate}
                onChange={handleDateChange}
                required
              />
            </div>
            <div>
              <label htmlFor="value">Enter value</label>
              <input
                id="value"
                type="number"
                value={formValue}
                onChange={handleValueChange}
                required
              />
            </div>
          </div>
          <button type="submit">Proceed</button>
        </form>
      </div>

      <div
        className={` ${
          successModalState ? styles.successModalWrapper : styles.hide
        } `}
      >
        <div className={styles.modalWrapper}>
          <div className={styles.iconWrapper}>
            <img src="./img/check.svg" alt="Success pic" />
          </div>
          <p className={styles.successText}>
            Your deal was submitted successfully!
          </p>
        </div>
      </div>
    </div>
  );
}
