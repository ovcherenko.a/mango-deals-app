import { Head } from "next/document";
import styles from "./Header.module.css";

export interface HeaderProps {
  modalHandler: () => void;
}

export default function Header({ modalHandler }: HeaderProps) {
  return (
    <header className={styles.header}>
      <div className={styles.logo_wrapper}>
        <img src="./img/logo.svg" alt="Mango Deals logo" />
        <p className="logo_text">mango deals</p>
      </div>

      <button onClick={modalHandler} className={styles.next_deal_btn}>
        new deal
      </button>
    </header>
  );
}
