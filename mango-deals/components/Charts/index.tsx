import React, { useState, useEffect } from "react";
import { LineChart, Line, CartesianGrid } from "recharts";

import { IChartListItem } from "../../types/types";

interface ChartProps {
  dataFromApi: IChartListItem[];
}

interface IChartItem {
  name: string;
  pv: number;
}

export default function Charts({ dataFromApi }: ChartProps) {
  const [chartElements, setChartElements] = useState<IChartItem[]>([]);

  useEffect(() => {
    let newChartElements: IChartItem[] = [];

    if (dataFromApi) {
      dataFromApi.forEach((el) => {
        newChartElements.push({
          name: el.date,
          pv: el.value,
        });
      });
    }

    setChartElements(newChartElements.reverse());
  }, [dataFromApi]);

  return (
    <>
      <LineChart width={375} height={200} data={chartElements}>
        <Line type="monotone" dataKey="pv" stroke="#3EAEFF" strokeWidth={2} />
        <CartesianGrid stroke="#343e4d" />
      </LineChart>
    </>
  );
}
