import type { NextApiRequest, NextApiResponse } from "next";
import { chartsDB } from "../../db/db";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    if (req.body.pageNum) {
      let chartresults = chartsDB.getAll();
      let chartresultsByPage = chartresults.slice(0, req.body.pageNum * 10);
      res.status(200).json({
        obj: chartresultsByPage,
      });
    } else {
      chartsDB.create(req.body.date, req.body.value);
      res.status(200).json({
        obj: req.body,
      });
    }
  } else if (req.method === "DELETE") {
    chartsDB.delete(req.body.id);
    res.status(200).json({
      obj: req.body,
    });
  }
}
