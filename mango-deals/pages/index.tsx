import styles from "../styles/Home.module.css";
import { useState, useEffect } from "react";

import { IChartListItem, IResponseDataType } from "../types/types";

import Header from "../components/Header";
import Charts from "../components/Charts";
import List from "../components/List";
import Modal from "../components/Modal";
import dataHandler from "../services/dataHandler";

export default function Home() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [successModalState, setSuccessModalState] = useState(false);
  const [dataFromApi, setDataFromApi] = useState<IChartListItem[]>([]);
  const [pageNum, setPageNum] = useState(1);

  useEffect(() => {
    firstLoadData();
  }, []);

  function firstLoadData() {
    try {
      dataHandler("POST", {
        pageNum: pageNum,
      })
        .then((data: any) => {
          return data.json();
        })
        .then((data: IResponseDataType) => {
          setDataFromApi(data.obj);
        });
    } catch (e) {
      console.error(e);
    }
  }

  function loadMoreData(): void {
    try {
      let newPageNum = pageNum + 1;
      dataHandler("POST", {
        pageNum: newPageNum,
      })
        .then((data: any) => {
          return data.json();
        })
        .then((data) => {
          setDataFromApi(data.obj);
          setPageNum(newPageNum);
        });
    } catch (e) {
      console.error(e);
    }
  }

  function addToList(formDate: string, formValue: string): void {
    try {
      dataHandler("POST", {
        date: formDate,
        value: formValue,
      }).then(() => {
        setSuccessModalState(true);
        firstLoadData();
      });
    } catch (e) {
      console.error(e);
    }
  }

  function deleteFromList(obj: IChartListItem) {
    try {
      dataHandler("DELETE", obj).then(() => {
        firstLoadData();
      });
    } catch (e) {
      console.error(e);
    }
  }

  function modalHandler() {
    setIsModalOpen(!isModalOpen);
  }

  return (
    <div className={styles.container}>
      <title>Mango Deals</title>
      <meta name="description" content="Mango Deals" />
      <link rel="icon" href="/favicon.ico" />
      <Modal
        isModalOpen={isModalOpen}
        addToList={addToList}
        modalHandler={modalHandler}
        successModalState={successModalState}
        setSuccessModalState={setSuccessModalState}
      />
      <div
        className={`${styles.unblurred} ${isModalOpen ? styles.blurred : ""}`}
      >
        <Header modalHandler={modalHandler} />
        <Charts dataFromApi={dataFromApi} />
        <List
          dataFromApi={dataFromApi}
          deleteFromList={deleteFromList}
          loadMoreData={loadMoreData}
        />
      </div>
    </div>
  );
}
