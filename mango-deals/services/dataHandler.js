
export default async function dataHadler(method, obj) {
    let url = './api'
    
    if(method === 'POST') {
        const res = await fetch(
            url,
            {
                method: method,
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(obj)
            }
        )
        return res;
    }

    if(method === 'DELETE') {
        const res = await fetch(
            url,
            {
                method: method,
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(obj)
            }
        )
        return res;
    }
}