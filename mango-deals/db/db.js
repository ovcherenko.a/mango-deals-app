const fs = require("fs");
let charts = require("./chartresults.json");

export const chartsDB = {
  getAll,
  create,
  delete: _delete,
};

function getAll() {
  return charts.sort((a, b) => {
    return new Date(b.date).getTime() - new Date(a.date).getTime();
  });
}

function create(date, value) {
  let chartItem = {};
  chartItem.id = Math.random().toString(16).slice(2);
  chartItem.date = date;
  chartItem.value = value;

  charts.push(chartItem);
}

function _delete(id) {
  charts = charts.filter((el) => el.id.toString() !== id);
  saveData();
}

function saveData() {
  fs.writeFileSync("./chartresults.json", JSON.stringify(charts, null, 4));
}
