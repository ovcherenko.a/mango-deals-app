export interface IChartListItem {
  id: string;
  date: string;
  value: number;
}

export interface IResponseDataType {
  obj: IChartListItem[];
}
